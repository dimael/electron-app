const openButton = document.querySelector('#openButton');
const destroyButton = document.querySelector('#destroyButton');

document.querySelector('#version').innerText = versions.electron();

openButton.addEventListener('click', function() {
  ipcRenderer.send('openButton:click', {
    dataField: 'someData',
  });
});

destroyButton.addEventListener('click', function () {
  console.log('click')
  ipcRenderer.send('destroyButton:click', {});
});
