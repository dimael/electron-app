const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const path = require('path');

const isDev = process.env.NODE_ENV !== 'development';
const isMac = process.platform === 'darwin';
let dialerWindow;

function createMainWindow() {
  const mainWindow = new BrowserWindow({
    title: 'My app',
    width: isDev ? 1200 : 500,
    height: 1000,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  if (isDev) {
    mainWindow.webContents.openDevTools();
  }

  mainWindow.loadFile(path.join(__dirname, './renderer/index.html'));
}

function createDialerWindow() {
  dialerWindow = new BrowserWindow({
    title: 'Dialer',
    width: 250,
    height: 400,
  });

  const options = {
    type: 'question',
    checkboxLabel: 'Don\'t ask again',
    title: 'Child window',
    message:
      'Do you want to close this window?',
    cancelId: 1,
    buttons: [ 'Close', 'Cancel' ],
    shouldConfirmBeforeClose: true,
  }
  const handler = async (event, window, options) => {
    console.log('[CLOSE]')
    const close = () => {
      console.log('[DESTROY]');
      window.destroy();
    };

    event.preventDefault();

    const { response } = await dialog.showMessageBox(window, options);
    console.log('response', response);
    if (response === options.cancelId) {
      console.log('[CANCEL]')
      return;
    }
    close();
  }

  dialerWindow.loadFile(path.join(__dirname, './renderer/dialer.html'));
  dialerWindow.on('close', (event) => handler(event, dialerWindow, options));
}

app.whenReady().then(() => {
  createMainWindow();

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createMainWindow()
    }
  })
});

app.on('window-all-closed', () => {
  if (!isMac) {
    app.quit();
  }
})

// Respond to ipcRenderer
ipcMain.on('openButton:click', (event, options) => {
  createDialerWindow();
  console.log('[OPEN]')
})

ipcMain.on('destroyButton:click', (event, options) => {
  dialerWindow.destroy();
  console.log('[DESTROY] by button')
})